/*
 * libusb example program to manipulate U.are.U 4000B fingerprint scanner.
 * Copyright © 2007 Daniel Drake <dsd@gentoo.org>
 *
 * Basic image capture program only, does not consider the powerup quirks or
 * the fact that image encryption may be enabled. Not expected to work
 * flawlessly all of the time.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <libusb.h>
//#include <libusbi.h>

#define MIN(a, b)	((a) < (b) ? (a) : (b))
#define MAX(a, b)	((a) > (b) ? (a) : (b))

#define F81534_VID_1		0x1934
#define F81534_VID_2		0x2C42
#define F81534_PID		0x1202

#define BIT(nr)			(1UL << (nr))

#define CTRL_IN		(LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_IN)
#define CTRL_OUT	(LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_OUT)
#define F81534_CMD			0xa0

#define F81534_UART_BASE_ADDRESS	0x1200
#define F81534_UART_OFFSET		0x10
#define F81534_DIVISOR_LSB_REG		(0x00 + F81534_UART_BASE_ADDRESS)
#define F81534_DIVISOR_MSB_REG		(0x01 + F81534_UART_BASE_ADDRESS)
#define F81534_INTERRUPT_ENABLE_REG	(0x01 + F81534_UART_BASE_ADDRESS)
#define F81534_FIFO_CONTROL_REG		(0x02 + F81534_UART_BASE_ADDRESS)
#define F81534_LINE_CONTROL_REG		(0x03 + F81534_UART_BASE_ADDRESS)
#define F81534_MODEM_CONTROL_REG	(0x04 + F81534_UART_BASE_ADDRESS)
#define F81534_LINE_STATUS_REG		(0x05 + F81534_UART_BASE_ADDRESS)
#define F81534_MODEM_STATUS_REG		(0x06 + F81534_UART_BASE_ADDRESS)
#define F81534_CLOCK_REG		(0x08 + F81534_UART_BASE_ADDRESS)
#define F81534_CONFIG1_REG		(0x09 + F81534_UART_BASE_ADDRESS)

#define UART_FCR		2	/* Out: FIFO Control Register */
#define UART_FCR_ENABLE_FIFO	0x01 /* Enable the FIFO */
#define UART_FCR_CLEAR_RCVR	0x02 /* Clear the RCVR FIFO */
#define UART_FCR_CLEAR_XMIT	0x04 /* Clear the XMIT FIFO */

#define UART_MCR		4	/* Out: Modem Control Register */
#define UART_MCR_LOOP		0x10 /* Enable loopback test mode */

#define UART_LSR		5	/* In:  Line Status Register */
#define UART_LSR_BI		0x10 /* Break interrupt indicator */

#define UART_MSR		6	/* In:  Modem Status Register */
#define UART_MSR_DCD		0x80 /* Data Carrier Detect */
#define UART_MSR_RI		0x40 /* Ring Indicator */
#define UART_MSR_DSR		0x20 /* Data Set Ready */
#define UART_MSR_CTS		0x10 /* Clear to Send */
#define UART_MSR_DDCD		0x08 /* Delta DCD */
#define UART_MSR_TERI		0x04 /* Trailing edge ring indicator */
#define UART_MSR_DDSR		0x02 /* Delta DSR */
#define UART_MSR_DCTS		0x01 /* Delta CTS */
#define UART_MSR_ANY_DELTA	0x0F /* Any of the delta bits! */

#define F81534_MAX_BUS_RETRY		9000
#define F81534_MAX_DATA_BLOCK		128
#define F81534_FLASH_BUSY_STATUS	0x03
#define F81534_SECTOR_SIZE		0x1000

#define READ_SIZE			9
#define F81534_CONFIG_ADDR		0x2F00
#define F81534_CONFIG_ADDR_OFFSET	0x0F00

#define F81534_CUSTOM_VALID_TOKEN	0xf0
#define F81534_RS232_FLAG		0x00
#define F81534_RS485_FLAG		0x01
#define F81534_RS485_1_FLAG		0x03
#define F81534_MODE_MASK		0x03

#define F81534_PORT_CONF_RS485		BIT(0)
#define F81534_PORT_CONF_RS485_INVERT	BIT(1)
#define F81534_PORT_CONF_DISABLE_PORT	BIT(3)
#define F81534_PORT_CONF_NOT_EXIST_PORT	BIT(7)

#define NO_CHANGE			(-1)

#define ERASE_COMMAND_ERROR		1
#define ERASE_EMPTY_ERROR		2
#define WRITE_DATA_ERROR		3
#define WRITE_DATA_COMPARE_ERROR	4
#define PARAMETER_ERROR			5
#define NO_MEM_ERROR			6
#define IO_ERROR			7
#define NO_ERROR_END			8
#define INIT_ERROR			9
#define SYSFS_ERROR			10
#define SYSFS_NOT_FOUND_ERROR		11

#define F81534_SYSFS			"/sys/bus/usb/drivers/f81534"
#define USB_SYSFS			"/sys/bus/usb/devices"
#define TTYUSB_TOKEN			"ttyUSB"

static int debug = 0;
static int m_listonly = 0;
#define DEBUG_MSG(x) \
	do { \
		if(debug)  \
			x; \
	}while(0)

#define CHECK_AND_RETURN(debug_on, xxx) \
	do { \
		int status_tmp = xxx; \
		if (status_tmp < 0) {\
			if (debug_on) \
				fprintf(stderr, "%s-%d %s error, status: %d\n", __func__, __LINE__, #xxx, status_tmp); \
			return status_tmp; \
		} \
	} while(0)

void usage_port_control(void)
{
	printf("F81532/534 Configurate Tool\n");
	printf
	    ("Parameters: -s <value> -e <value> -g <value> -u <value> -d -l -t <ttyUSBx>\n");
 	printf("\t-t, --target <ttyUSBx>: x is port number\n");
 	printf("\t-g, --gpio <value>: GPIO value[0-7]\n");
 	printf("\t-m, --mode <value>: UART Mode[1-3]\n");
 	printf("\t\t 1: RS232\n");
 	printf("\t\t 2: RS485\n");
 	printf("\t\t 3: RS485 Invert\n");
 	printf("\t-s, --set_mode <value>: UART/GPIO Mode[1-4]\n");
 	printf("\t\t 1: RS232 => GPIO mode: 1, UART mode: 1\n");
 	printf("\t\t 2: RS485 => GPIO mode: 3, UART mode: 2\n");
 	printf("\t\t 3: RS485 Invert => GPIO mode: 2, UART mode: 3\n");
 	printf("\t\t 4: RS422 => GPIO mode: 0, UART mode: don't care\n");
 	printf("\t\t    -s(set_mode) cannot use with -g/-m\n");
 	printf("\t-d : dump current setting\n");
 	printf("\t-l : list all F81534 devices\n");
	printf("\n");
#if 0	
	printf("\nExpert Mode (Dangerous, becarefully!!):\n");
	printf("\t-n, --node <usb-path>: listed from -l\n");
	printf("\t-e, --enable <value>: Enable[1]/Disable[0]\n");
	printf("\t-i, --index <value>: spec port\n");
	printf("\n");
#endif
}

static int f81534_setregister(libusb_device_handle * dev_handle,
			      int reg, unsigned char data)
{
	int r =
	    libusb_control_transfer(dev_handle, CTRL_OUT, F81534_CMD, reg, 0,
				    &data,
				    1, 0);
	if (r < 0) {
		fprintf(stderr, "%s error %d\n", __func__, __LINE__);
	}

	return r;
}

static int f81534_getregister(libusb_device_handle * dev_handle,
			      int reg, unsigned char *data)
{
	int r = libusb_control_transfer(dev_handle, CTRL_IN, F81534_CMD, reg, 0,
					data,
					1, 0);
	if (r < 0) {
		fprintf(stderr, "%s error %d\n", __func__, __LINE__);
	}

	return r;
}

int f81534_select_device(libusb_device_handle ** handle, char *device_node)
{
	libusb_device **devs;
	libusb_device *dev;
	int i = 0, j, len, count;
	int status;
	char buf[32];
	char tmp[32];

	status = libusb_get_device_list(NULL, &devs);
	if (status < 0)
		return status;

	*handle = NULL;

	status = SYSFS_NOT_FOUND_ERROR;
	while ((dev = devs[i++]) != NULL) {
		struct libusb_device_descriptor desc;

		status = libusb_get_device_descriptor(dev, &desc);
		if (status < 0) {
			fprintf(stderr, "failed to get device descriptor");
			return status;
		}

		len = 0;
		len = sprintf(&buf[len], "%d", libusb_get_bus_number(dev));

		count = libusb_get_port_numbers(dev, tmp, sizeof(tmp));
		if (count > 0) {
			len += sprintf(&buf[len], "-%d", tmp[0]);
			for (j = 1; j < count; j++)
				len += sprintf(&buf[len], ".%d", tmp[j]);
			sprintf(tmp, "%s:1.0", buf);
		}

		status = SYSFS_NOT_FOUND_ERROR;

		if (!strcmp(device_node, tmp)) {
			DEBUG_MSG(printf("%s: selected %s\n", __func__, tmp));
			status = libusb_open(dev, handle);
			break;
		}
	}

	libusb_free_device_list(devs, 1);
	return status;
}

static int f81534_command_delay(libusb_device_handle * dev_handle)
{
	unsigned int count = F81534_MAX_BUS_RETRY;
	unsigned char tmp;

	do {
		CHECK_AND_RETURN(debug,
				 f81534_getregister(dev_handle, 0x1003, &tmp));

		if (tmp & 0x03)
			continue;

		if (tmp & 0x04)
			break;

	} while (--count);

	if (!count) {
		fprintf(stderr, "%s: max retry exceed !!!\n", __func__);
		return IO_ERROR;
	}

	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1003, tmp & ~0x04));
	return 0;
}

static int f81534_read_data(libusb_device_handle * dev_handle,
			    int address, int size, unsigned char *buf)
{
	unsigned int count = 0;
	unsigned int read_size = 0;
	unsigned int block = 0;

	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug, f81534_setregister(dev_handle, 0x1002, 0x03));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 16) & 0xff));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 8) & 0xff));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 0) & 0xff));

	/* continous read mode */
	do {
		read_size = MIN(F81534_MAX_DATA_BLOCK, size);
		for (count = 0; count < read_size; ++count) {
			CHECK_AND_RETURN(debug,
					 f81534_command_delay(dev_handle));
			if ((size <= F81534_MAX_DATA_BLOCK) && (read_size == (count + 1)))	// last data
				CHECK_AND_RETURN(debug,
						 f81534_setregister(dev_handle,
								    0x1001,
								    0xf1));
			else
				CHECK_AND_RETURN(debug,
						 f81534_setregister(dev_handle,
								    0x1002,
								    0xf1));
			CHECK_AND_RETURN(debug,
					 f81534_command_delay(dev_handle));
			CHECK_AND_RETURN(debug,
					 f81534_getregister(dev_handle, 0x1004,
							    &buf[count +
								 (block *
								  F81534_MAX_DATA_BLOCK)]));
			CHECK_AND_RETURN(debug,
					 f81534_command_delay(dev_handle));
		}

		size -= read_size;
		block += 1;
	} while (size);

	return 0;
}

static int f81534_write_data(libusb_device_handle * dev_handle,
			     int address, int size, unsigned char *buf)
{
	unsigned int count = 0;
	unsigned int write_size = 0;
	unsigned int block = 0;

	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug, f81534_setregister(dev_handle, 0x1001, 0x06));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug, f81534_setregister(dev_handle, 0x1002, 0x02));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 16) & 0xff));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 8) & 0xff));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 0) & 0xff));
	do {
		write_size = MIN(F81534_MAX_DATA_BLOCK, size);
		for (count = 0; count < write_size; ++count) {
			CHECK_AND_RETURN(debug,
					 f81534_command_delay(dev_handle));
			if ((size <= F81534_MAX_DATA_BLOCK) && (write_size == (count + 1)))	// last data
				CHECK_AND_RETURN(debug,
						 f81534_setregister(dev_handle,
								    0x1001,
								    buf
								    [count +
								     block *
								     F81534_MAX_DATA_BLOCK]));
			else
				CHECK_AND_RETURN(debug,
						 f81534_setregister(dev_handle,
								    0x1002,
								    buf
								    [count +
								     block *
								     F81534_MAX_DATA_BLOCK]));
		}

		size -= write_size;
		block += 1;
	} while (size);
	return 0;
}

static int f81534_erase_sector(libusb_device_handle * dev_handle, int address)
{
	unsigned char current_status = 0;
	unsigned int count = F81534_MAX_BUS_RETRY;

	/* erase */
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug, f81534_setregister(dev_handle, 0x1001, 0x06));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug, f81534_setregister(dev_handle, 0x1002, 0x20));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 16) & 0xff));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1002,
					    (address >> 8) & 0xff));
	CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
	CHECK_AND_RETURN(debug,
			 f81534_setregister(dev_handle, 0x1001,
					    (address >> 0) & 0xff));
	/* getting status */
	while (--count) {
		CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
		CHECK_AND_RETURN(debug,
				 f81534_setregister(dev_handle, 0x1002, 0x05));
		CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
		CHECK_AND_RETURN(debug,
				 f81534_setregister(dev_handle, 0x1001, 0xff));
		CHECK_AND_RETURN(debug, f81534_command_delay(dev_handle));
		CHECK_AND_RETURN(debug,
				 f81534_getregister(dev_handle, 0x1004,
						    &current_status));
		if (!(F81534_FLASH_BUSY_STATUS & current_status)) {
			DEBUG_MSG(printf("%s: data:%x, count:%d, ok\n",
					 __func__, current_status, count));
			break;
		}
	}

	return 0;
}

#if 0
static int main_process(libusb_device_handle * dev_handle)
{
	int ret;
	int i;
	unsigned char data[128];

	ret = f81534_read_data(dev_handle, 0x3000, sizeof(data), data);
	if (ret) {
		fprintf(stderr, "%s: error - %d\n", __func__, ret);
	}

	printf("\n");
	for (i = 0; i < sizeof(data); ++i) {
		printf("%02x ", data[i]);
	}
	printf("\n");

	return ret;
}
#endif

int parser_string(int *data, char *ptr)
{
	char *tmp;

	if (!data)
		return PARAMETER_ERROR;

	*data = strtol(ptr, &tmp, 16);
	if (*tmp) {
		printf("error: %s\n", ptr);
		return PARAMETER_ERROR;
	}

	return 0;
}

int find_f81534_with_tty(char *tty, char *device_node, int *idx)
{
	int i = 0, selected = -1, count = 0;
	char buf[64];
	char port[4][9] = {0};

	if (!device_node || !idx || !tty)
		return PARAMETER_ERROR;

	sprintf(buf, "%s/%s/", F81534_SYSFS, device_node);

#if 1
	struct dirent **namelist;
	int n;

	n = scandir(buf, &namelist, 0, alphasort);
	if (n < 0)
		return 0;	

	for (i = 0; i < n; ++i) {
		if (strncmp(namelist[i]->d_name, TTYUSB_TOKEN,
				strlen(TTYUSB_TOKEN)))
			continue;

		if (m_listonly)
			printf("\t%s\n", namelist[i]->d_name);
		
		strcpy(port[count], namelist[i]->d_name);

		if (!strcmp(tty, namelist[i]->d_name))
			selected = count;

		++count;
	}

	for (i = 0; i < n; i++)
		free(namelist[i]);

	free(namelist);
#else
	DIR *d;
	struct dirent *dir;

	d = opendir(buf);
	if (!d) {
		fprintf(stderr, "Cant open %s\n", buf);
		return SYSFS_ERROR;
	}

	memset(port, 0, sizeof(port));

	while ((dir = readdir(d)) != NULL) {
		if (!strncmp(dir->d_name, TTYUSB_TOKEN, strlen(TTYUSB_TOKEN))) {
			if (m_listonly)
				printf("\t%s\n", dir->d_name);

			strncpy(port[i], dir->d_name, sizeof(port[i]) - 1);
			if (!strcmp(tty, dir->d_name))
				selected = i;
			++i;
		}
	}

	closedir(d);
#endif

	if (selected == -1)
		return SYSFS_NOT_FOUND_ERROR;

	*idx = selected;
	return 0;
}

int get_string(char *sysfs, char *buf, int len)
{
	int fd;
	int size;

	if (!buf || len <= 4)
		return PARAMETER_ERROR;

	fd = open(sysfs, O_RDONLY);
	if (fd < 0)
		return IO_ERROR;

	size = read(fd, buf, len);
	if (size <= 0) {
		close(fd);
		return IO_ERROR;
	}

	buf[size - 1] = 0;	/* remove \n */
	close(fd);
	return 0;
}

int get_vid_string(char *device_node, char *buf, int len)
{
	int l;
	char path[64], tmp[64];

	l = sprintf(path, "%s/%s", USB_SYSFS, device_node);
	path[l - 4] = 0;
	sprintf(tmp, "%s/idVendor", path);

	return get_string(tmp, buf, len);
}

int get_pid_string(char *device_node, char *buf, int len)
{
	int l;
	char path[64], tmp[64];

	l = sprintf(path, "%s/%s", USB_SYSFS, device_node);
	path[l - 4] = 0;
	sprintf(tmp, "%s/idProduct", path);

	return get_string(tmp, buf, len);
}

int get_product_string(char *device_node, char *buf, int len)
{
	int l;
	char path[64], tmp[64];

	l = sprintf(path, "%s/%s", USB_SYSFS, device_node);
	path[l - 4] = 0;
	sprintf(tmp, "%s/product", path);

	return get_string(tmp, buf, len);
}

int find_f81534(char *tty, char *device_node, int *idx)
{
	int r = SYSFS_NOT_FOUND_ERROR;
	DIR *d;
	struct dirent *dir;
	char buf[128];
	char pid[8], vid[8];

	if (!tty || !idx || !device_node)
		return PARAMETER_ERROR;

	sprintf(buf, "%s/", F81534_SYSFS);

	d = opendir(buf);
	if (!d) {
		fprintf(stderr, "Cant open %s\n", buf);
		return SYSFS_ERROR;
	}

	while ((dir = readdir(d)) != NULL) {
		if (strlen(dir->d_name) >= 2 && dir->d_name[1] == '-') {
			memset(pid, 0, sizeof(pid));
			memset(vid, 0, sizeof(vid));

			get_vid_string(dir->d_name, vid, sizeof(vid) - 1);
			get_pid_string(dir->d_name, pid, sizeof(pid) - 1);
			get_product_string(dir->d_name, buf, sizeof(buf) - 1);

			if (m_listonly)
				printf("%s - (node:%s vid:%s, pid:%s)\n", buf,
				       dir->d_name, vid, pid);

			r = find_f81534_with_tty(tty, dir->d_name, idx);
			if (!r && !m_listonly) {
				strcpy(device_node, dir->d_name);
				break;
			}
		}
	}

	closedir(d);

	if (m_listonly)
		return 0;

	return r;
}

static int f81534_check_port_hw_disabled(struct libusb_device_handle *devh,
		int phy)
{
	int status;
	unsigned char old_mcr;
	unsigned char msr;
	unsigned char lsr;
	unsigned char msr_mask;

	msr_mask = UART_MSR_DCD | UART_MSR_RI | UART_MSR_DSR | UART_MSR_CTS;

	status = f81534_getregister(devh,
				F81534_MODEM_STATUS_REG + phy * 0x10, &msr);
	if (status < 0)
		return status;
	
	if ((msr & msr_mask) != msr_mask)
		return 0;

	status = f81534_setregister(devh,
				F81534_FIFO_CONTROL_REG + phy * 0x10,
				UART_FCR_ENABLE_FIFO | UART_FCR_CLEAR_RCVR |
				UART_FCR_CLEAR_XMIT);
	if (status < 0)
		return status;

	status = f81534_getregister(devh,
				F81534_MODEM_CONTROL_REG + phy * 0x10,
				&old_mcr);
	if (status < 0)
		return status;

	status = f81534_setregister(devh,
				F81534_MODEM_CONTROL_REG + phy * 0x10,
				UART_MCR_LOOP);
	if (status < 0)
		return status;

	status = f81534_setregister(devh,
				F81534_MODEM_CONTROL_REG + phy * 0x10, 0x0);
	if (status < 0)
		return status;

	usleep(60000);

	status = f81534_getregister(devh,
				F81534_LINE_STATUS_REG + phy * 0x10, &lsr);
	if (status < 0)
		return status;

	status = f81534_setregister(devh,
				F81534_MODEM_CONTROL_REG + phy * 0x10,
				old_mcr);
	if (status < 0)
		return status;

	if ((lsr & UART_LSR_BI) == UART_LSR_BI)
		return -ENODEV;

	return 0;
}

int f81534_change_setting(struct libusb_device_handle *devh, int idx,
			  int expert, int enable, int mode, int gpio,
			  int dump_only)
{
	int status = PARAMETER_ERROR, i, cnt = 0;
	int hw_disabled[4] = {0};
	unsigned char buf[READ_SIZE], buf_bak[READ_SIZE];

	for (i = 0; i < 4; ++i) {
		hw_disabled[i] = f81534_check_port_hw_disabled(devh, i);
		//printf("need_skip: %d, %d\n", i, hw_disabled[i]);
	}
	
	printf("Dump original data\n");

	status = f81534_read_data(devh, F81534_CONFIG_ADDR, READ_SIZE, buf);
	if (status) {
		fprintf(stderr, "%s: f81534_read_data failed, status:%d\n",
			__func__, status);
		return status;
	}

	memcpy(buf_bak, buf, sizeof(buf));

	if (dump_only) {
		for (i = 0; i < 4; ++i) {
			if (buf[i + 1] & F81534_PORT_CONF_NOT_EXIST_PORT)
				continue;

			printf("Port%d:", cnt);

			if (buf[i + 1] & F81534_PORT_CONF_DISABLE_PORT)
				printf("Disabled ");

			if (buf[i + 1] & F81534_PORT_CONF_RS485) {
				printf("RS485 ");

				if (buf[i + 1] & F81534_PORT_CONF_RS485_INVERT)
					printf("Invert ");
			} else {
				printf("RS232 ");
			}

			printf("GPIO: %d\n", buf[i + 5]);
			++cnt;
		}

		return 0;
	}

	/* Find idx to save data */
	if (expert) {
		i = idx;
	} else {
		cnt = 0;

		for (i = 0; i < 4; ++i) {
			if (hw_disabled[i])
				continue;
			
			if (buf[i + 1] & (F81534_PORT_CONF_NOT_EXIST_PORT |
					  F81534_PORT_CONF_DISABLE_PORT))
				continue;

			if (cnt == idx)
				break;
			++cnt;
		}

		if (i == 4 || cnt == 4) {
			fprintf(stderr, "%s: find idx error\n", __func__);
			return PARAMETER_ERROR;
		}
	}

	DEBUG_MSG(printf("%s: i: %d\n", __func__, i));

	if (mode != NO_CHANGE) {
		buf[i + 1] &= ~F81534_MODE_MASK;
		switch (mode) {
		case 1:	/* RS232 */
			printf("Mode: change to RS232\n");
			break;
		case 2:	/* RS485 */
			printf("Mode: change to RS485\n");
			buf[i + 1] |= F81534_RS485_FLAG;
			break;

		case 3:	/* Invert */
			printf("Mode: change to RS485 Invert\n");
			buf[i + 1] |= F81534_RS485_1_FLAG;
			break;
		}

	}

	if (gpio != NO_CHANGE) {
		printf("GPIO: change to %d\n", gpio);
		buf[i + 5] = gpio;
	}

	if (expert && enable != NO_CHANGE) {
		buf[i + 1] &= ~F81534_PORT_CONF_DISABLE_PORT;
		buf[i + 1] |= enable ? 0 : F81534_PORT_CONF_DISABLE_PORT;
	}

	if (!memcmp(buf, buf_bak, sizeof(buf))) {
		printf("The same. Not change anything.\n");
		return 0;
	}

	printf("Erasing ...\n");
	status = f81534_erase_sector(devh, F81534_CONFIG_ADDR & ~0x0fff);
	if (status) {
		fprintf(stderr, "%s: f81534_erase_sector error!!\n", __func__);
		return ERASE_COMMAND_ERROR;
	}

	usleep(500000);

	printf("Writing ...\n");

	status = f81534_write_data(devh, F81534_CONFIG_ADDR, READ_SIZE, buf);
	if (status) {
		fprintf(stderr, "%s: f81534_write_data error!!\n", __func__);
		return WRITE_DATA_ERROR;
	}

	usleep(500000);
	printf("Completed !!\n");

	return 0;
}

int f81534_normal_mode(char *tty, int mode, int gpio, int dump)
{
	int r, idx;
	char device_node[64];
	struct libusb_device_handle *devh = NULL;

	memset(device_node, 0, sizeof(device_node));
	r = find_f81534(tty, device_node, &idx);
	if (r) {
		fprintf(stderr, "find_f81534 error: %d\n", r);
		return r;
	}

	if (m_listonly)		/* list only */
		return 0;

	DEBUG_MSG(printf("tty:%s, in: %s, idx:%d\n", tty, device_node, idx));

	if (mode == NO_CHANGE && gpio == NO_CHANGE) {
		printf("Nothing to be changed\n");
		return r;
	}

	r = f81534_select_device(&devh, device_node);
	if (r) {
		fprintf(stderr, "f81534_select_device error: %d\n", r);
		return r;
	}

	r = libusb_set_auto_detach_kernel_driver(devh, 1);
	if (r) {
		fprintf(stderr,
			"%s: not support detach kernel driver :%d\n",
			__func__, r);
		return r;
	}

	r = libusb_claim_interface(devh, 0);
	if (r < 0) {
		fprintf(stderr, "%s: claim interface error %d\n", __func__, r);
		return r;
	}
#if 0
	r = f81534_change_setting(devh, idx, mode, gpio, dump);
	if (r < 0) {
		fprintf(stderr, "%s: claim interface error %d\n", __func__, r);
		return r;
	}
#endif
	DEBUG_MSG(printf("%s: clean-up\n", __func__));
	libusb_release_interface(devh, 0);
	libusb_close(devh);
	return 0;
}

int main(int argc, char *argv[])
{
	struct libusb_device_handle *devh = NULL;
	int r = 0, dump = 0, expert = 0;
	int opt, idx = NO_CHANGE;
	int enable = NO_CHANGE, gpio = NO_CHANGE, mode = NO_CHANGE, set_mode = NO_CHANGE;
	char tty[32], device_node[32];
	static char *short_opt = "he:g:m:dlt:n:i:s:";
	static struct option long_options[] = {
		{"enable", required_argument, NULL, 'e'},
		{"node", required_argument, NULL, 'n'},
		{"index", required_argument, NULL, 'i'},
		{"gpio", required_argument, NULL, 'g'},
		{"mode", required_argument, NULL, 'm'},
		{"dump", no_argument, NULL, 'd'},
		{"list", no_argument, NULL, 'l'},
		{"help", no_argument, NULL, 'h'},
		{"target", required_argument, NULL, 't'},
		{"set_mode", required_argument, NULL, 's'},
	};

	optind = 0;
	memset(tty, 0, sizeof(tty));
	memset(device_node, 0, sizeof(device_node));

	while ((opt =
		getopt_long_only(argc, argv, short_opt, long_options,
				 NULL)) != -1) {
		switch (opt) {
		case 'i':
			r = parser_string(&idx, optarg);
			if (idx < 0 || idx >= 4)
				r = PARAMETER_ERROR;
			break;

		case 'n':
			strcpy(device_node, optarg);
			break;

		case 'e':
			r = parser_string(&enable, optarg);
			if (enable < 0 || enable > 1)
				r = PARAMETER_ERROR;
			break;

		case 'g':
			if (set_mode != NO_CHANGE) {
				r = PARAMETER_ERROR;
				break;
			}
			
			r = parser_string(&gpio, optarg);
			if ((gpio < 0 || gpio > 7) && gpio != 0x80)
				r = PARAMETER_ERROR;
			break;

		case 'm':
			if (set_mode != NO_CHANGE) {
				r = PARAMETER_ERROR;
				break;
			}

			r = parser_string(&mode, optarg);
			if (mode < 1 || mode > 3)
				r = PARAMETER_ERROR;
			break;

		case 'd':
			dump = 1;
			break;

		case 'l':
			m_listonly = 1;
			break;

		case 't':
			strcpy(tty, optarg);
			break;
		case 's':
			if (gpio != NO_CHANGE || mode != NO_CHANGE) {
				r = PARAMETER_ERROR;
				break;
			}

			r = parser_string(&set_mode, optarg);
			if (set_mode < 1 || set_mode > 4)
				r = PARAMETER_ERROR;

			break;
		case 'h':
		case '?':
		default:
			usage_port_control();
			r = PARAMETER_ERROR;
		}

		if (r)
			break;
	}

	if (r) {
		usage_port_control();
		return PARAMETER_ERROR;
	}

	if (set_mode != NO_CHANGE) {
		switch (set_mode) {
		case 1: gpio = 1; mode = 1; break;
		case 2: gpio = 3; mode = 2; break;
		case 3: gpio = 2; mode = 3; break;
		case 4: gpio = 0; mode = NO_CHANGE; break;
		}
	}

	if (!m_listonly) {
		if (!strlen(tty) && (!strlen(device_node) || idx == NO_CHANGE)) {
			usage_port_control();
			return PARAMETER_ERROR;
		}
	}

	r = libusb_init(NULL);
	if (r < 0) {
		fprintf(stderr, "failed to initialise libusb\n");
		return INIT_ERROR;
	}

	do {
		if (strlen(tty) || m_listonly) {
			printf("Processing wih Normal Mode\n");

			memset(device_node, 0, sizeof(device_node));
			r = find_f81534(tty, device_node, &idx);
			if (r) {
				fprintf(stderr,
					"find_f81534 error: %d\n", r);
				return r;
			}

			if (m_listonly)	/* list only */
				break;

			DEBUG_MSG(printf
				  ("tty:%s, in: %s, idx:%d\n", tty, device_node,
				   idx));

		} else {
			printf("Processing wih Expert Mode\n");
			expert = 1;
		}

		if (!dump) {
			if (mode == NO_CHANGE && gpio == NO_CHANGE
			    && enable == NO_CHANGE) {
				printf("Nothing to be changed\n");
				break;
			}
		}

		r = f81534_select_device(&devh, device_node);
		if (r) {
			fprintf(stderr, "f81534_select_device error: %d\n", r);
			break;
		}

		r = libusb_set_auto_detach_kernel_driver(devh, 1);
		if (r) {
			fprintf(stderr,
				"%s: not support detach kernel driver :%d\n",
				__func__, r);
			return r;
		}

		r = libusb_claim_interface(devh, 0);
		if (r < 0) {
			fprintf(stderr, "%s: claim interface error %d\n",
				__func__, r);
			break;
		}

		r = f81534_change_setting(devh, idx, expert, enable, mode, gpio,
					  dump);
		if (r < 0) {
			fprintf(stderr, "%s: change setting error %d\n",
				__func__, r);
			break;
		}
	} while (0);

	if (devh) {
		DEBUG_MSG(printf("%s: clean-up\n", __func__));
		libusb_release_interface(devh, 0);
		libusb_close(devh);
	}

	libusb_exit(NULL);
	return r;
}
